<?php
//include_once '../../../../Source/bitm/seip14/Semester/Semester.php';

include_once '../../../../vendor/autoload.php';

use App\bitm\seip14\Semester\Semester;

$ob = new Semester();
$AlData = $ob->index();
if (!empty($AlData)) {
    ?>

    <html>

        <head>
            <title>Index|Data</title>  
        </head>

        <body>
            <br/>
            <a href="Create.php">Form page</a>
            <br/> 
            <table border="1">
                <tr>
                    <th>SL</th>
                    <th>Name</th>
                    <th>Semester</th>
                    <th>Offer</th>
                    <th>Cost</th>
                    <th>W</th>
                    <th>Total Fee</th>
                    <th colspan="3">Action</th>
                </tr>
                <?php
                $serialize = 01;
                foreach ($AlData as $SingleData) {
                    ?>
                    <tr>
                        <td><?php echo $serialize++; ?></td>
                        <td><?php echo $SingleData['name'] ?></td>
                        <td><?php echo $SingleData['semester'] ?></td>
                        <td><?php echo $SingleData['offer'] ?></td>
                        <td><?php echo $SingleData['cost'] ?></td>
                        <td><?php echo $SingleData['w'] ?></td>
                        <td><?php echo $SingleData['total'] ?></td>
                        <td><a href="Show.php?id=<?php echo $SingleData['id'] ?>">View</a></td>
                        <td><a href="Edit.php?id=<?php echo $SingleData['id'] ?>">Edit</a></td>
                        <td><a href="Delete.php?id=<?php echo $SingleData['id'] ?>">Delete</a></td>
                    </tr>
                    <?php
                }
                ?>
            </table>
        </body>


    </html>
    <?php
} 
?>
    

