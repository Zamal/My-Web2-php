
<?php
error_reporting(E_ALL);
error_reporting(E_ALL & ~E_DEPRECATED);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
date_default_timezone_set('Europe/London');

if(PHP_SAPI== 'cli')
    die('this example should only web browser');

require_once '../../../../vendor/phpoffice/phpexcel/Classes/PHPExcel.php';
require_once ('../../../../vendor/autoload.php');
 use App\bitm\seip14\actor\Actor;
    $obj= new Actor();
    $allData = $obj->index(); 
    
    //create new PHPExcel object
    $objPHPExcel= new PHPExcel();
    $objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
            ->setLastModifiedBy("Maarten Balliauw")
            ->setTitle("Office 2007 XLSX Test Document")
            ->setSubject("Office 2007 XLSX Test Document")
            ->setDescription("Tes document for office 2007 XLSX,  Generated Using PHP classes.")
            ->setKeywords("Office 2007 openxml php")
            ->setCategory("Test result file");
    
    //add some data
    $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1','SL')
            ->setCellValue('B1','ID')
            ->setCellValue('C1','Book Title');
    $counter=2;
    $serial=0;
    foreach($allData as $data){       
        $serial++;
        $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A'.$counter, $serial)
                ->setCellValue('B'.$counter, $data['name'])
                ->setCellValue('C'.$counter, $data['message']);
        $counter++;
    }
    
    //Rename worksheet
    $objPHPExcel->getActiveSheet()->setTitle('actor_list');
    
    //set active sheet index to the first sheet, so excel opens this as the first sheet
    $objPHPExcel->setActiveSheetIndex(0);
    
    //Redirect output to a clients web browser (Excel5)
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment; filename="01simple.xls"');
    header('Cache-Control: max-age=0');
    
    //if you'r saving to IE 9, then the following may be nedded
    header('Cache-Control: max-age=1');
    
     //if you'r saving to IE ove SSL, then the following may be nedded
    header('Expires:Mon, 26 Jul 1997 05:00:00 GMT');// Date In the past
    header('Last-Modified: '.gmdate('D, d M Y H:i:s').'GMT');// allwayse modify
    header('Cache-Control:cache, must-revalidate'); //HTTP/1.1
    header('Pragma:public'); //HTTP/1.0
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
    $objWriter->save('php://output');
    exit;