<html>
    <head>
        <title> isset variable </title>
    </head>
    <body>
        <?php
        
        $a = array('test' =>1, 'hello' => NULL ,'pie' => array ('a' => 24,'b' =>10));
        
        var_dump(isset($a['test']));
        echo "<br>";
        var_dump(isset($a['too']));
        echo "<br>";
        var_dump(isset($a['hello']));
        echo "<br>";
        var_dump(isset($a['pie']['a']));
        echo "<br>";
        var_dump(isset($a['pie']['b']));
        echo "<br>";
        var_dump(isset($a['cake']['a']));
       
        ?>
    </body>
</html>
