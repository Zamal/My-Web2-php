<html>
    <head>
        <title> unset variable </title>
    </head>
    
    <body>  
    <?php
    function foo()
    {
        static $bar;
        $bar++;
        echo "Before unset: $bar, ";
        unset($bar);
        $bar = 23;
        echo "after unset: $bar\n";
    }

    foo();
    echo "<br>";
    foo();
    echo "<br>";
    foo();
    ?>

</body>
</html>

