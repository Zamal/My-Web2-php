<?php

class Calculator{
    
    public $number1="";
    public $number2="";
    
    public function prepare($data){
        $this->number1=$data['num1'];
        $this->number2=$data['num2'];
        return $this;
    }
    public function add(){
       echo "total result of add ".($this->number1+$this->number2)."<br/>";
       return $this;
    }
    public function sub(){
       echo "total result of sub ".($this->number1-$this->number2)."<br/>";
       return $this;
    }
    public function mul(){
       echo "total result of mul ".($this->number1*$this->number2);
    }
}
