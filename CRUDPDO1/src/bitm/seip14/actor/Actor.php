<?php

namespace App\bitm\seip14\actor;

use PDO;

class Actor {

    public $id = '';
    public $name = '';
    public $message = '';
    public $data = '';
    public $pdouser = 'root';
    public $pdopass = '';
    public $conn = '';

    public function __construct() {

        session_start();

        $this->conn = new PDO('mysql:host=localhost;dbname=actor', $this->pdouser, $this->pdopass);
    }

    public function prepare($data = '') {

        if (!empty($data['actor_name'])) {
            $this->name = $data['actor_name'];
        } else {
            $_SESSION['actempty'] = "Actor name Requerd";
        }
        if (!empty($data['message'])) {
            $this->message = $data['message'];
        } else {
            $_SESSION['msgempty'] = "Message  Requerd";
        }
        if (array_key_exists('id', $data)) {
            $this->id = $data['id'];
        }
        $_SESSION['Form_data'] = $data;
    }

    public function store() {
        $query = "INSERT INTO actor_name(id, unique_id, name, message)
    VALUES(:id, :u_id, :name, :msg)";
        $statement = $this->conn->prepare($query);
        $statement->execute(array(
            ":id" => null,
            ":u_id" => uniqid(),
            ":name" => $this->name,
            ":msg" => $this->message,
        ));
        header('location:index.php');
    }

    public function index() {
        $qr="SELECT * FROM `actor_name`";
        $query = $this->conn->prepare($qr);
        $query->execute();

        while ($result = $query->fetch(PDO::FETCH_ASSOC)) {
            $this->data[] = $result;
        }
        return $this->data;
    }

    public function show() {
           
    $query = "SELECT * FROM `actor_name` WHERE unique_id=". "'".$this->id."'";
      $query = $this->conn->prepare($query);
      $query->execute();
      
      $row = $query->fetch(PDO::FETCH_ASSOC);
      return $row;   
    
    }

    public function update() {
        
      $query = "UPDATE `actor`.`actor_name` SET `name` = '$this->name',`message` = ' $this->message' WHERE `actor_name`.`unique_id` =". "'".$this->id."'";
      $query = $this->conn->prepare($query);
      $query->execute();     
     
      if($query->fetch(PDO::FETCH_ASSOC))
    {              
    $_SESSION['Message']="Successfully updated";
    } 
    header('location:Create.php');
    }

    public function delete() {         
    
    $query = "DELETE FROM `actor_name` WHERE `unique_id` = "."'".$this->id."'";
      $query = $this->conn->prepare($query);
      $query->execute();
      
      $query->fetch(PDO::FETCH_ASSOC);
      
     header('location:index.php');
     
    }

    }
