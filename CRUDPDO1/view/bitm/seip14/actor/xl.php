<?php
error_reporting(E_ALL);
error_reporting(E_ALL &~E_DEPRECATED);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
date_default_timezone_set('Europe/London');
if(PHP_SAPI=='cli')
    die('this example should only web browser');

require_once ('../../../../vendor/phpoffice/phpexcel/Classes/PHPExcel.php');
require_once ('../../../../vendor/autoload.php');
 use App\bitm\seip14\actor\Actor;
    $obj=new Actor();
    $allData = $obj->index();
    $objPHPExcel=new PHPExcel();
    $objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
            ->setLastModifiedBy("Maarten Balliauw")
            ->setTitle("Office 2007 XLSX Test document")
            ->setSubject("Office 2007 XLSX Test document")
            ->setDescription("Tes document for office 2007 XLSX,  Generated PHP classes.")
            ->setKeywords("Office 2007 openxml php")
            ->setCategory("Test result file");
    //add some data
    $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1','SL')
            ->setCellValue('B1','Name')
            ->setCellValue('C1','Message');
    $counter=2;
    $serial=0;
    foreach ($allData as $data){       
        $serial++;
        $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A'.$counter,$serial)
                ->setCellValue('B'.$counter,$data['name'])
                ->setCellValue('C'.$counter,$data['message']);
        $counter++;
    }
    $objPHPExcel->getActiveSheet()->setTitle('Actor_name');
    $objPHPExcel->setActiveSheetIndex(0);
    header('Content-Type: applicalton/vnd.ms-excel');
    header('Content-Disposition:attachment;filename="01simple.xls"');
    header('Cache-Control:max-age=0');
    header('Cache-Control:max-age=1');
    header('Expires:Mon,26 jul 1997 05:00:00 GMT');
    header('Last-Modified: '.  gmdate('D,d M Y H:i:s').'GMT');
    header('Cache-Control:cache,must-revalidate');
    header('Pragma:public');
    $objWriter=  PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel5');
    $objWriter->save('php://output');
    exit;