<?php

    //include_once '../../../../src/bitm/seip14/actor/actor.php';
    include_once ("../../../../vendor/autoload.php");
    use App\bitm\seip14\actor\Actor;

    $editobject=new Actor();

    $editobject->prepare($_GET);
    $single=$editobject->show();
    if(isset($single) && !empty($single)){
    if(isset($_SESSION['message']) && !empty($_SESSION['message'])){
    echo $_SESSION['message'];
    unset($_SESSION['message']);
    }
?>

    <html>
    <head>
      <title>Update Actor name:</title>
    </head>
    <body>
    <fieldset>
        <legend>Actor form:</legend>
        <form method="POST" action="update.php">
            <label>Actor Name:</label>
            <input type="text" name="actor_name" value="<?php echo $single['name']; ?>"><br/>            
            <label>your message</label>
            <input type="text" name="message" value="<?php echo $single['message']; ?>"><br/><br/>            
            <input type="hidden" name="id" value="<?php echo $_GET['id'] ?>">
            <input type="submit" value="update">
        </form>
     </fieldset>
     <a href="index.php">View all data</a>

    </body>
   </html>
<?php
}else{
  $_SESSION['Errorsmsg'] = "You not access right data";
   header('location:errors.php');  
}
?>
