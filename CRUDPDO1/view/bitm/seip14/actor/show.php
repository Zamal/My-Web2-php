<?php
    //include_once '../../../../src/bitm/seip14/actor/actor.php';
    include_once ("../../../../vendor/autoload.php");
    use App\bitm\seip14\actor\Actor;
    $obj = new Actor();

    $obj->prepare($_GET);
    $onedata=$obj->show();

    if(isset($onedata) && !empty($onedata)){
 ?>
   <table border="1" align="center">
    <tr>
        <th>SL</th>
        <th>Name</th>   
        <th>Message</th>
        <th>Unique_Id</th>
        
    </tr>
    
    <tr>
        <td><?php echo $onedata['id']; ?></td>
        <td><?php echo $onedata['name']; ?></td>
        <td><?php echo $onedata['message']; ?></td> 
        <td><?php echo $onedata['unique_id']; ?></td>
    </tr>
   
  </table>
  <a href="index.php">view all data</a>
 <?php } else{
    
    $_SESSION['Errorsmsg'] = "Invalid Id";
    header('location:errors.php');    
 }
 ?>