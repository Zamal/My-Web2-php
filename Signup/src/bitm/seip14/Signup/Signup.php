<?php
namespace App\bitm\seip14\Signup;
use PDO;
class Signup {
    
    public $username='';
    public $firstname='';
    public $lastname='';
    public $password='';
    public $repassword='';
    public $data='';
    public $alldata='';
    public $email='';
    public $id='';
    public $conn='';
    public $uname='root';
    public $passwrd='';
    public $verification_id = '';
    //
    public $fullname='';
    public $fathername='';
    public $mothername='';
    public $birthdate='';
    public $gender='';
    public $mobile='';
    public $occapation='';
    public $education='';
    public $religion='';
    public $merital='';
    public $currentstatus='';
    public $nationality='';
    public $interest= '';
    public $bio='';
    public $nid='';
    public $passportnum='';
    public $skillarea='';
    public $language='';
    public $bloodgroup='';
    public $faxnum='';
    public $height='';
    public $addres='';
    public $weburl='';
    public $profilepic='';
    public $image='';
    public $other='';
   
    
    

    public function __construct() {
        
        session_start();  
        date_default_timezone_set('Asia/Dhaka');
    
        $this->conn = new PDO("mysql:host=localhost;dbname=form", $this->uname, $this->passwrd);
        // set the PDO error mode to exception
        $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);   
        
    }    
   
    public function prepare( $data='')
                { 
//        echo "<pre>";
//        print_r($data);
//        die();
          if(!empty($data['username'])){     
        $this->username=$data['username'];
          }
          if(!empty($data['firstname'])){     
        $this->firstname=$data['firstname']; 
          }
           if(!empty($data['lastname'])){     
        $this->lastname=$data['lastname']; 
          }
          if(!empty($data['password'])){     
        $this->password=  md5($data['password']); 
          } 
          if(!empty($data['re_password'])){     
         $this->repassword=md5($data['re_password']); 
          }
          if(!empty($data['email'])){     
         $this->email=$data['email']; 
          }
          if (!empty($data['vid'])) {
            $this->verification_id = $data['vid'];
        }
        if (!empty($data['fullname'])) {
            $this->fullname=$data['fullname'];
        }
        if (!empty($data['fathername'])) {
             $this->fathername=$data['fathername'];
        }
         if (!empty($data['mothername'])) {
             $this->mothername=$data['mothername']; 
        }
        if (!empty($data['birthdate'])) {
              $this->birthdate=$data['birthdate'];  
        }
        if (!empty($data['gender'])) {
              $this->gender=$data['gender'];  
        }
        if (!empty($data['mobile'])) {
              $this->mobile=$data['mobile'];  
        }
        if (!empty($data['occapation'])) {
              $this->occapation=$data['occapation'];  
        }
         if (!empty($data['edstatus'])) {
              $this->education=$data['edstatus'];  
        }
        if (!empty($data['religion'])) {
              $this->religion=$data['religion'];  
        }
        if (!empty($data['merital'])) {
              $this->merital=$data['merital'];  
        }
        if (!empty($data['currentstatus'])) {
              $this->currentstatus=$data['currentstatus'];  
        }
        if (!empty($data['nationality'])) {
              $this->nationality=$data['nationality'];  
        }
           
        if (!empty($data['biography'])) {
              $this->bio=$data['biography'];  
        }
        if (!empty($data['passport'])) {
              $this->passportnum=$data['passport'];  
        }
        if (!empty($data['faxnum'])) {
              $this->faxnum=$data['faxnum'];  
        }
        if (!empty($data['height'])) {
              $this->height=$data['height'];  
        }
                 
        if (!empty($data['nid'])) {
              $this->nid=$data['nid'];  
        }
        if (!empty($data['address'])) {
              $this->addres=$data['address'];  
        }
        if (!empty($data['weburl'])) {
              $this->weburl=$data['weburl'];  
        }
        if (!empty($data['other'])) {
              $this->other=$data['other'];  
        }
        if(!empty($data['image'])){     
        $this->image = $data['image'];
          }
           
           
       if(array_key_exists('id', $data))
                {
               $this->id =$data['id']; 
            }
            $_SESSION['data']=$data;            
        return $this;
    }
    public function index(){
        
        $qr="SELECT * FROM `signup`";
        $query = $this->conn->prepare($qr);
        $query->execute();

        while ($result = $query->fetch(PDO::FETCH_ASSOC)) {
            $this->data[] = $result;
        }
        return $this->data;
    }
     public function show()
    {
        $query = "SELECT * FROM profiles WHERE user_id=".$_SESSION['user']['id'];
        $STH = $this->conn->prepare($query);
        $STH->execute();

        $result = $STH->fetch(PDO::FETCH_ASSOC);
        return $result;

    }
    
     public function signup(){ 
       
        try {
             $usrname = "'$this->username'";
            $qr = "SELECT * FROM signup WHERE user_name=" . $usrname;
            $STH = $this->conn->prepare($qr);
            $STH->execute();
            $user = $STH->fetch(PDO::FETCH_ASSOC);

            $email = "'$this->email'";
            $qr = "SELECT * FROM signup WHERE email=" . $email;
            $STH = $this->conn->prepare($qr);
            $STH->execute();
            $user2 = $STH->fetch(PDO::FETCH_ASSOC);
            if (!empty($user)) {
                $_SESSION['username'] = "Username already exists";
                header('location:register.php');
            } elseif (!empty($user2)) {
                $_SESSION['username'] = "Already register with this email";
                header('location:register.php');
            }  else {
           $verification_code = uniqid();
           $query="INSERT INTO signup(id, unique_id, verification_id, user_name, firstname, lastname, password, email, is_active, is_admin, created, modify, deleted)
           VALUES(:id, :u_id, :v_id, :uname, :fname, :lname, :pword, :email, :is_active, :is_admin, :created, :modify, :deleted)";
        
        $statement = $this->conn->prepare($query);
        $result =$statement->execute(array(
         ":id" => NULL,
         ":u_id" => uniqid(),
         ":v_id" => $verification_code,
         ":uname" => $this->username,
         ":fname" => $this->firstname,
         ":lname" => $this->lastname,
         ":pword" =>  $this->password,
         ":email" => $this->email,
         ":is_active" =>"0",
         ":is_admin" =>"0",
         ":created" => date("Y-m-d H:i:s"),
         ":modify" =>'',
         ":deleted" =>'',
            )
            );
        $_SESSION['success']="Successfully Registration"; 
        $msg = "Click the below link for verify your email address.<br/> http://zamalhossain.cse@gmail.com/Views/verify.php?vid=$verification_code";
              
                $msg = wordwrap($msg, 70);
                mail("$this->email", "zamalhossain.cse@gmail.com", $msg);
                $last_id = $this->conn->lastInsertId();
                
                $query="INSERT INTO profiles(id, user_id)VALUES(:id, :u_id)";                 
                $statement = $this->conn->prepare($query);
                $statement->execute(array(
                 ":id" => NULL,
                 ":u_id" => $last_id ,
            )
            );
                
            header('location:register.php');
            }
         } catch (Exception $ex) {
           echo 'Error: ' . $e->getMessage();
         }
            }        
    
    public function login()
    {
        $usrname = "'$this->username'";
        $password = "'$this->password'";
        $qr = "select * from signup where user_name =  $usrname && password = $password";
        $STH = $this->conn->prepare($qr);
        $STH->execute();
        $user = $STH->fetch(PDO::FETCH_ASSOC);
//        echo "<pre>";
//        print_r($user);
//        die();
        if (isset($user) && !empty($user)) {
            if ($user['is_active'] == 0) {
                $_SESSION['Message'] = "<h3>Your account not verified yet. Check your email and verify</h3>";
                header('location:login.php');
            } else {
                $_SESSION['user'] = $user;               
                $_SESSION['loginmsg'] ="login successfully";
                header('location:index.php');
            }
        } else {
            $_SESSION['Message'] = "<h3>invalid username or password</h3>";
            header('location:login.php');
        }

    }
    
    public function verification()
    {
        $verification_code = "'" . $this->verification_id . "'";
        $qr = "SELECT * FROM users WHERE verification_id = $verification_code";
        $STH = $this->conn->prepare($qr);
        $STH->execute();
        $user = $STH->fetch(PDO::FETCH_ASSOC);
//        echo "<pre>";
//        print_r($user);
//        die();

        if (isset($user['verification'])) {
            $verification_code = "'" . $this->verification_id . "'";
            $query = "UPDATE users SET is_active = 1 WHERE verification_id =" . $verification_code;
            $STH = $this->conn->prepare($query);
            if ($STH->execute()) {
                $_SESSION['Message'] = "<h1>You're verified now. Thank you !</h1>";
                header('location:login.php');
            }
        }
    }
    
//    public function getSession(){
//        return @$_SESSION['login'];
//    }

     public function profileupdate(){
                   
            $query = "UPDATE profiles SET full_name = :fn, father_name = :fnn, mother_name = :fnn, birthdate = :bdate, gender = :gender , "
                    . "mobile_num = :mobile, occapation = :0cc, education_status = :edstatus, religion = :religion,"
                    . "merital_status = :mstatus, current_status = :cstatus,"
                    . "nationality = :nationality, biography = :bio, national_id = :nid, passport_num = :pnum,"
                    . "fax_num = :fnum, height = :height, addresses = :addres, web_url = :wurl, other = :other,"
                    . "modify = :modify WHERE user_id =".$_SESSION['user']['id'];
//            echo $query;
//            die();
            $stmt = $this->conn->prepare($query);
            $stmt->execute(array(
                ':fn' => $this->fullname,
                ':fnn' => $this->fathername,
                ':fnn' => $this->mothername,
                ':bdate' => $this->birthdate,
                ':gender' => $this->gender,
                 ':mobile' => $this->mobile,
                ':0cc' => $this->occapation,
                ':edstatus' => $this->education,
                ':religion' => $this->religion,
                ':mstatus' => $this->merital,
                ':cstatus' => $this->currentstatus,
                ':nationality' => $this->nationality,
                ':bio' => $this->bio,
                ':nid' => $this->nid,
                ':pnum' => $this->passportnum,
                ':fnum' => $this->faxnum,
                ':height' => $this->height,
                ':addres' => $this->addres,
                ':wurl' => $this->weburl,
                ':other' => $this->other,
                 ":modify" => date("Y-m-d H:i:s"),
                ));
            $_SESSION['Message'] = "Profile Successfully updated";
            header('location:index.php');
       
    }
}
