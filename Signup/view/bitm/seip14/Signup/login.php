
<?php 
include_once '../../../../vendor/autoload.php';
use App\bitm\seip14\Signup\Signup;
$obj=new Signup();
//include("include/header.php");
?>


<!DOCKTYPE HTML>
<html>
    <head>
    <div id="title">
        <title>Login Form</title>
    </div>
    <link rel="stylesheet" type="text/css" href="style.css">
    </head>
    <body id="l_body-color">
        <div id="l_warpar">
            <div id="l_head3">
                <h3>Login Form</h3>
            </div>            
            <div id="l_content">
                <div id="l_head2">
                    <h2>Login Now</h2>
                </div>            
                           
                <div id="l_login_reg">               
                    <form method="POST" action="login_proccess.php">
                        <div id="l_message">
                        <h3>
                        <?php 
                        if(!empty( $_SESSION['Message'])){                                                
                        echo "<span style=center>". $_SESSION['Message']."</span>";
                        unset( $_SESSION['Message']);                                      
                      }
                      ?>
                       </h3>
                        </div>
                    <table border="0">                        
                            <tr>
                                <td>Enter username</td>
                                <td>                                
                                    <input type="text" name="username" size="40" placeholder="username"><?php 
                                        if(!empty($_SESSION['username'])){    
                                        echo $_SESSION['username'];
                                        unset($_SESSION['username']);                                      
                                      }
                                      ?>
                                </td>
                            </tr>                        
                            
                            <tr>
                                <td>Enter Password</td>
                                <td>                                
                                    <input type="password" name="password" size="40" placeholder="password"><?php 
                                        if(!empty($_SESSION['password'])){    
                                        echo $_SESSION['password'];
                                        unset($_SESSION['password']);                                      
                                     } ?>                                    
                                </td>
                            </tr>                            
                            
                            <tr>
                                <td colspan="2"> 
                                    <span style="float: right;">
                                    <input type="submit" name="submit" value="Login">                                   
                                    </span>
                                </td>
                            </tr>                                                 
                        </table>                    
                </form>
                </div>                
                <div id="l_back">                    
                    <a href="register.php"><img src="img/back.png" alt="back" /></a>                    
                </div>                
              <div id="l_footer">
                <h2>https://gitlab.com/Zamal/My-Web2-php</h2>                
              </div>
        </div>
    </body>
</html>