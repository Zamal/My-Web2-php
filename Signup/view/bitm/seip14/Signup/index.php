<?php

include_once '../../../../vendor/autoload.php';
use App\bitm\seip14\Signup\Signup;
$obj = new Signup();
$alluser = $obj->index();
//echo "<pre>";
//print_r($alluser);
//die();
if(isset($_SESSION['user'] ) && !empty($_SESSION['user'] )){

?>
<html>
    <head>
        <title>Profile Edit</title>
         <link rel="stylesheet" type="text/css" href="style.css">
    </head>
    <body id="I_body-color">         
        <div id="I_warpar">            
        <div id="I_content">         
              
            <div id="I_link">
                <ul>
                    <li><a href="index.php">Home Page</a></li>
                    <li><a href="show.php">Profile</a></li>                     
                    <li><a href="logout.php">Logout</a></li>
                    <li><a href="changepassword.php">Change Password</a></li>
                </ul>
            </div>
            <div id="I_head3">
                <h3><?php echo "Welcome our website"; ?></h3>
            </div>
            <p id="I_loginmsg">
                <?php 
                if(!empty($_SESSION['loginmsg'])){
                    echo $_SESSION['loginmsg'];
                    unset($_SESSION['loginmsg']);
                }
                ?>
            </p>            
            <div id="I_head2">
                <h2>All User List</h2>
            </div>
            <div id="I_table">
            <table border="1" align="center">
                   
                <tr>
                  <th>Id</th>  
                  <th>Name</th>
                  <th>Email</th>
                  <th colspan="3">Profile</th>  
                </tr>
                <?php                
                foreach ($alluser as $user){
                    if($user['id']==$_SESSION['user']['id']){
                ?>
                
                <tr>
                    <td><?php echo $user['id']; ?></td>
                    <td><?php echo $user['user_name'];?></td>
                    <td><?php echo $user['email'];?></td>
                    <td><a href="profileedit.php?id=<?php echo $user['id']?>">View profile</td>    
                </tr>
                    <?php }
                    
                    }?>
                
            </table>
            </div>
        </div>
            <div id="I_footer">
            <h3>https://gitlab.com/Zamal/My-Web2-php</h3>
        </div>
        </div>        
    </body>
</html>
<?php }  else {
    $_SESSION['Message'] = "Login for continue";
    header('location:login.php');
} ?>