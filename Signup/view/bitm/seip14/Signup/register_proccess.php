<?php 
include_once '../../../../vendor/autoload.php';
use App\bitm\seip14\Signup\Signup;
$obj=new Signup();


if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    if(empty($_POST['username'])){
        $_SESSION['username']="Username must be Required!";
        header('location:register.php');
    }elseif(strlen($_POST['username'])<3 || strlen($_POST['username'])>8){
        $_SESSION['username']="user name must be 3-8 charecter!";
        header('location:register.php');
    }elseif(empty($_POST['firstname'])) {
        $_SESSION['firstname']="user name must be required!";
        header('location:register.php');
    }elseif(empty($_POST['lastname'])) {
        $_SESSION['lastname']="last name must be required!";
        header('location:register.php');
    }elseif(empty($_POST['password'])) {
        $_SESSION['password']="password must be required!";
        header('location:register.php');
    }elseif(strlen($_POST['password'])<6 || strlen($_POST['password'])>12) {
        $_SESSION['password']="password  must be 6-12 charecter!";
        header('location:register.php');
    }elseif($_POST['password']!== $_POST['re_password']) {
        $_SESSION['passwordm']="password  not match!";
        header('location:register.php');    
    }elseif(empty($_POST['email'])) {
        $_SESSION['email']="email must be required!!";
        header('location:register.php');
    }elseif(!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)){
        $_SESSION['email'] = "Enter a  valid email";
        header('location:register.php');
        }else{
         $obj->prepare($_POST);
         $obj->signup();
        }
}


