<?php

include_once '../../../../vendor/autoload.php';
use App\bitm\seip14\Signup\Signup;
$obj = new Signup();
//echo "<pre>";
//print_r($_SESSION['user']);
//die();
//$myid = $_SESSION['user']['id'];
// $obj->prepare($myid );
    $onedata=$obj->show();
//    echo "<pre>";
//    print_r($onedata);
//    die();

    if(isset($onedata) && !empty($onedata)){
 ?>
<html>
    <head>
        <title>Profile</title>
         <link rel="stylesheet" type="text/css" href="style.css">
    </head>
    <body id="S_bodycolor">
        <div id="S_head2">
            <h2>Profile</h2>
        </div>
        <div id="S_table">
   <table border="1"> 
       <tr>           
           <td>image:</td>
           <td><?php echo $onedata['image']; ?></td>           
       </tr> 
       <tr>           
           <td>Full Name:</td>
           <td><?php echo $onedata['full_name']; ?></td>           
       </tr>       
       <tr>
           <td>Father Name:</td>
           <td><?php echo $onedata['father_name']; ?></td>
       </tr>
       <tr>
           <td>Mother Name:</td>
           <td><?php echo $onedata['mother_name']; ?></td>
       </tr>
       <tr>
           <td>Date Of Birth</td>
           <td><?php echo $onedata['birthdate']; ?></td>
       </tr>
       <tr>
           <td>Gender</td>
           <td><?php echo $onedata['gender']; ?></td>
       </tr>
       <tr>
           <td>Mobile Number:</td>
           <td><?php echo $onedata['mobile_num']; ?></td>
       </tr>
       <tr>
           <td>Occapation:</td>
           <td><?php echo $onedata['occapation']; ?></td>
       </tr>
       <tr>
           <td>Education Status:</td>
           <td><?php echo $onedata['education_status']; ?></td>
       </tr>
       <tr>
           <td>Religion:</td>
           <td><?php echo $onedata['religion']; ?></td>
       </tr>
       <tr>
           <td>Merital Status:</td>
           <td><?php echo $onedata['merital_status']; ?></td>
       </tr>
       <tr>
           <td>Current Status:</td>
           <td><?php echo $onedata['current_status']; ?></td>
       </tr>
       <tr>
           <td>Nationality:</td>
           <td><?php echo $onedata['nationality']; ?></td>
       </tr>
       <tr>
           <td>Biography:</td>
           <td><?php echo $onedata['biography']; ?></td>
       </tr>
       <tr>
           <td>National Id Number:</td>
           <td><?php echo $onedata['national_id']; ?></td>
       </tr>
       <tr>
           <td>Passport Number:</td>
           <td><?php echo $onedata['passport_num']; ?></td>
       </tr>
       <tr>
           <td>Fax Number:</td>
           <td><?php echo $onedata['fax_num']; ?></td>
       </tr>
       <tr>
           <td>Height:</td>
           <td><?php echo $onedata['height']; ?></td>
       </tr>
       <tr>
           <td>Addresses:</td>
           <td><?php echo $onedata['addresses']; ?></td>
       </tr>
       <tr>
           <td>Web URL Link:</td>
           <td><?php echo $onedata['web_url']; ?></td>
       </tr>
       <tr>
           <td>Other</td>
           <td><?php echo $onedata['other']; ?></td>
       </tr>    
  </table>
   </div>
   <div id="S_link">       
        <ul>
            <li><a href="index.php">Back To Home page</a></li>                   
            <li><a href="profileedit.php">Edit Profile</a></li>                
           
        </ul>
  </div>
 <?php } else{
    
    $_SESSION['Errorsmsg'] = "Invalid Id";
    header('location:errors.php');    
 }
 ?>
  <div id="S_footer">
      <h2>https://gitlab.com/Zamal/My-Web2-php</h2>
  </div>
  </body>
</html>

