<?php
include_once '../../../../vendor/autoload.php';
use App\bitm\seip14\Signup\Signup;
$obj=new Signup();


if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    if(empty($_POST['username'])){
        $_SESSION['username']="Username must be Required!";
    header('location:login.php');    
    }elseif(empty($_POST['password'])) {
        $_SESSION['password']="password must be required!";
    header('location:login.php');    
    }  else {
        $obj->prepare($_POST);
        $obj->login();
    }
}