<?php
include_once '../../../../vendor/autoload.php';
use App\bitm\seip14\Signup\Signup;
$obj=new Signup();
$mydata = $obj->show();
//echo "<pre>";
//print_r($mydata);

?>
<html>
    <head>
        <title>Profile Edit</title>
         <link rel="stylesheet" type="text/css" href="style.css">
    </head>
    <body id="p_body-color">                
        <div id="p_warpar">            
        <div id="p_content">
            <div id="p_link">
                <ul>
                    <li><a href="index.php">Home Page</a></li>                   
                    <li><a href="Register.php">Register</a></li>                    
                    <li><a href="logout.php">Logout</a></li>
                    <li><a href="changepassword.php">Change Password</a></li>
                </ul>
            </div>
        <div id="p_head3">
            <h3>Now Can You Fill up Your Profile </h3>
        </div>
            
        <div id="p_login_reg">
            <form method="POST" action="profileupdate.php" enctype="multipart/form-data">
                <table border="0">                                     
                                                  
                      <input type="file" name="image"/>                         
                     <tr>
                        <td>Full name:</td>
                        <td>                            
                            <input type="text" name="fullname" placeholder="enter your fullname" value="<?php 
                            if(!empty($mydata['full_name'])){
                                echo $mydata['full_name'];
                            }
                            ?>"> 
                        </td>
                    </tr> 
                    <tr>
                        <td>Father name:</td>
                        <td>                            
                            <input type="text" name="fathername" placeholder="enter your father name" value="<?php 
                            if(!empty($mydata['father_name'])){
                                echo $mydata['father_name'];
                            }
                            ?>"> 
                        </td>
                    </tr> 
                    <tr>
                        <td>Mother name:</td>
                        <td>                            
                            <input type="text" name="mothername" placeholder="enter your mother name" value="<?php 
                            if(!empty($mydata['mother_name'])){
                                echo $mydata['mother_name'];
                            }
                            ?>"> 
                        </td>
                    </tr> 
                    <tr>
                        <td>Date of Birth:</td>
                        <td>                            
                            <input type="text" name="birthdate" placeholder="enter your birthdate" value="<?php 
                            if(!empty($mydata['birthdate'])){
                                echo $mydata['birthdate'];
                            }
                            ?>"> 
                        </td>
                    </tr> 
                    <tr>
                        <td>Your gender:</td>
                        <td>                        
                            <input type="text" name="gender" placeholder="enter your gender" value="<?php 
                            if(!empty($mydata['gender'])){
                                echo $mydata['gender'];
                            }
                            ?>"> 
                        </td>
                    </tr>
                    <tr>
                        <td>Your Mobile num:</td>
                        <td>                        
                            <input type="number" name="mobile" placeholder="enter your mobile number" value="<?php 
                            if(!empty($mydata['mobile_num'])){
                                echo $mydata['mobile_num'];
                            }
                            ?>"> 
                        </td>
                    </tr>
                    <tr>
                        <td>Your Occapation:</td>
                        <td>                        
                            <input type="text" name="occapation" placeholder="enter your occapation" value="<?php 
                            if(!empty($mydata['occapation'])){
                                echo $mydata['occapation'];
                            }
                            ?>"> 
                        </td>
                    </tr>
                    <tr>
                        <td>Your Education status:</td>
                        <td>                        
                            <input type="text" name="edstatus" placeholder="enter your education status" value="<?php 
                            if(!empty($mydata['education_status'])){
                                echo $mydata['education_status'];
                            }
                            ?>"> 
                        </td>
                    </tr>
                    <tr>
                        <td>Your Religion:</td>
                        <td>                        
                            <input type="text" name="religion" placeholder="enter your religion" value="<?php 
                            if(!empty($mydata['religion'])){
                                echo $mydata['religion'];
                            }
                            ?>"> 
                        </td>
                    </tr>
                    <tr>
                        <td>Your merital status:</td>
                        <td>                        
                            <input type="text" name="merital" placeholder="enter your merital status" value="<?php 
                            if(!empty($mydata['merital_status'])){
                                echo $mydata['merital_status'];
                            }
                            ?>"> 
                        </td>
                    </tr>
                    <tr>
                        <td>Your current status:</td>
                        <td>                        
                            <input type="text" name="currentstatus" placeholder="enter your current status" value="<?php 
                            if(!empty($mydata['current_status'])){
                                echo $mydata['current_status'];
                            }
                            ?>"> 
                        </td>
                    </tr>
                    <tr>
                        <td>Nationality:</td>
                        <td>                        
                            <input type="text" name="nationality" placeholder="enter your nationality" value="<?php 
                            if(!empty($mydata['nationality'])){
                                echo $mydata['nationality'];
                            }
                            ?>"> 
                        </td>
                    </tr>
                    <tr>
                        <td>Biography:</td>
                        <td>                        
                            <input type="text" name="biography" placeholder="enter your biography" value="<?php 
                            if(!empty($mydata['biography'])){
                                echo $mydata['biography'];
                            }
                            ?>"> 
                        </td>
                    </tr>
                    <tr>
                        <td>National Id Num:</td>
                        <td>                        
                            <input type="number" name="nid" placeholder="enter your National Id Number" value="<?php 
                            if(!empty($mydata['national_id'])){
                                echo $mydata['national_id'];
                            }
                            ?>"> 
                        </td>
                    </tr>
                    <tr>
                        <td>Passport Num:</td>
                        <td>                        
                            <input type="number" name="passport" placeholder="enter your Passport Number" value="<?php 
                            if(!empty($mydata['passport_num'])){
                                echo $mydata['passport_num'];
                            }
                            ?>"> 
                        </td>
                    </tr>
                    <tr>
                        <td>Fax Num:</td>
                        <td>                        
                            <input type="number" name="faxnum" placeholder="enter your Fax Number" value="<?php 
                            if(!empty($mydata['fax_num'])){
                                echo $mydata['fax_num'];
                            }
                            ?>"> 
                        </td>
                    </tr>
                    <tr>
                        <td>Height:</td>
                        <td>                        
                            <input type="text" name="height" placeholder="enter your Height" value="<?php 
                            if(!empty($mydata['height'])){
                                echo $mydata['height'];
                            }
                            ?>"> 
                        </td>
                    </tr>
                    <tr>
                        <td>Addresses:</td>
                        <td>                        
                            <input type="text" name="address" placeholder="enter your Addresses" value="<?php 
                            if(!empty($mydata['addresses'])){
                                echo $mydata['addresses'];
                            }
                            ?>"> 
                        </td>
                    </tr>
                    <tr>
                        <td>Web URL:</td>
                        <td>                        
                            <input type="text" name="weburl" placeholder="enter your Web URL" value="<?php 
                            if(!empty($mydata['web_url'])){
                                echo $mydata['web_url'];
                            }
                            ?>"> 
                        </td>
                    </tr>
                    <tr>
                        <td>Other:</td>
                        <td>                        
                            <input type="text" name="other" placeholder="enter your other" value="<?php 
                            if(!empty($mydata['other'])){
                                echo $mydata['other'];
                            }
                            ?>"> 
                        </td>
                    </tr>
                    <tr>
                        <td>                            
                            <span style="float:right;"> <input type="submit" name="submit" value="update"></span>                            
                        </td>
                    </tr>
                </table>
            </form>
            <div id="p_footer">
                <h3>https://gitlab.com/Zamal/My-Web2-php</h3>                
              </div>
        </div> 
        </div>
        </div>
    </body>
</html>
